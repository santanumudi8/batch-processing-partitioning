package com.javatechie.spring.batch.config;

import org.springframework.batch.item.ItemProcessor;

import com.javatechie.spring.batch.entity.Customer;

public class CustomerProcessor implements ItemProcessor<Customer, Customer>{

	@Override
	public Customer process(Customer customer) throws Exception {
		
		return customer;
		
		/*
		if(customer.getCountry().equals("India")) { // this section of code is for filter the data
													// based on some condition which will be saved 
													// to database
			return customer;
		}else {
			return null;
		}
		*/
	}

}
